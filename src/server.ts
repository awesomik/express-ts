import App from './app';
import IndexRoute from './routes/index.route';
import PingRoute from './routes/ping.route';
import UsersRoute from './routes/users.route';

const app = new App([new IndexRoute(), new UsersRoute(), new PingRoute()]);

app.listen();
