import request from 'supertest';
import { Response } from 'supertest';
import App from '../app';
import IndexRoute from '../routes/index.route';
import PingRoute from '../routes/ping.route';

let server;
let indexRoute, pingRoute;
beforeAll(() => {
  indexRoute = new IndexRoute();
  pingRoute = new PingRoute();
  const routes = [indexRoute, pingRoute];
  const app = new App(routes);

  server = app.getServer();
});

afterAll(async () => {
  await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
});

describe.only('Testing default routes', () => {
  describe('[GET] /', () => {
    it('should get response statusCode 200', () => {
      return request(server)
        .get(`${indexRoute.path}`)
        .expect(200);
    });

    it('get response to a ping', async () => {
      const param = 'pong';

      return request(server)
        .get(`/ping/${param}`)
        .expect(200)
        .then((res: Response) => {
          const { data } = res.body;
          expect(data).toEqual(param);
        })
        .catch((err) => console.error(err));
    });

    it('should fail with 404 with unknow route', async () => {
      return request(server)
        .get('/unknown')
        .expect(404);
    });
  });
});
