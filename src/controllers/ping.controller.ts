import { NextFunction, Request, Response } from 'express';

class PingController {

  public index = async(req: Request, res: Response, next:NextFunction): Promise<void> => {
    try {
      const pongVal = req.params.pong;
      res.status(200).json({ message: 'ok', data: pongVal });
    }
    catch (error) {
      next(error);
    }
  }
};

export default PingController;
