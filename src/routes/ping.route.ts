import { Router } from 'express';
import PingController from '../controllers/ping.controller';
import Route from '../interfaces/routes.interface';

class PingRoute implements Route {
  public path = '/ping';
  public router = Router();
  public pingController = new PingController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/:pong`, this.pingController.index);
  }
}

export default PingRoute;
